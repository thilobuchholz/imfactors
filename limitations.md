# Limitations
This is a non-exhaustive list of limitations the current repository has. It can simultaneously be understood as a to-do list and as invitation to contributions!

-----

* This project at the moment is not based on broad empirical data, but by means of a thought-experiment tries to showcase a possible model of relevant variables and their relationships.
* It is focused on modelling applications which are mainly characterised by their direct messaging and group messaging features and built around text-based communication. e.g., Facebook Messenger, WhatsApp, Telegram, Threema, Signal, XMPP, Matrix, Briar, Tox, Wire, Viber. (cf. Instagram, Snapchat, YouTube)
* For some instant messaging services, different interface implementations can be used in order to use the same service or protocol. There are no reflections on the choice for different interfaces at this point of time.

