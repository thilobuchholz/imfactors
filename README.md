# imfactors

This repository aims to identify and collect knowledge on users' choices in the field of instant messaging services.

The project originally emerged from a modelling exercise within a Maastricht University course. The results of the findings of this modelling exercise are now shared within this repository in order to serve as a basis for further debate and investigation in the different factors shaping our collective perspective on different instant messaging services.

## Motivation
In the early 21st century, the ICT sector found itself at the epicentre of a digital revolution. After the early introduction of email protocols and the text messages, instant messenger services started taking off, and have since continuously grown in different formats and contexts (Desjardins, 2016). Recent trends include ever-increasing digital security and communication security threats through government agencies’ mass surveillance (Stahl, 2016), corporate data exploitation (Cabañas et al., 2018) and data and identity theft (Grabosky, 2016). In this context, the idea that people also establish a distrust against insecure, unencrypted, and centralised messaging services might not seem too far-fetched. However, the usage of instant messaging services in Europe still remains nearly undisputedly confined to two centralized services, WhatsApp and Facebook Messenger (We Are Social, 2019).

This project is motivated by investigating how such popularity can be explained, and exploring the potential for secure, privacy-focused, and decentralised instant messaging services to find wider adaptation. In short: Can the cultural phenomenon of instant messenger usage be modelled with regards to understanding the service choice of users and improving the attractiveness of secure, privacy-focused, decentralised messaging services?

## Usage

The file imfactors.graphml represents the graph containing all variables deemed relevant as well as the respective connections between those variables, and clusters those variables can be put into.

It can be accessed by graph editors supporting Graph Markup Language such as [yEd](https://www.yworks.com/products/yed) or [yEd Live](https://www.yworks.com/products/yed-live).

## Findings
You can find the list of variables currently included in the graph under [variables.md](variables.md). For the analysis of the graph see and contribute to [findings.md](findings.md).

![Visualization of the graph in PNG format](imfactors-visualization.png)

## References
* Cabañas, J. G., Cuevas, Á., & Cuevas, R. (2018). Unveiling and quantifying facebook exploitation of sensitive personal data for advertising purposes. In 27th {USENIX} Security Symposium ({USENIX} Security 18) (pp. 479-495).
* Desjardins, J. (2016). The Evolution of Instant Messaging [Blog Post, Infographic]. Retrieved from https://www.visualcapitalist.com/evolution-instant-messaging/
* Grabosky, P. (2016). The evolution of cybercrime, 2006–2016. In Cybercrime through an interdisciplinary lens (pp. 29-50). Routledge.
* Stahl, T. (2016). Indiscriminate mass surveillance and the public sphere. Ethics and Information Technology, 18(1), 33-39.
* We Are Social (2019). Digital in 2019 [Presentation]. Retrieved from https://wearesocial.com/global-digital-report-2019