# Findings
This file contains the main takeaways from the analysis of the current network.

![Visualization of the graph](imfactors-visualization.png)

## Network topology analysis
This section provides a network topology analysis of the model using the approach of whole networks as introduced in Haythornwaite (1996).

### Clusters
In the constructed network, the 56 variables are distributed in four moderately dense clusters with varying size (min: 5 nodes, max: 21 nodes) and an average cluster size of 14 nodes. Given the variables they contain, the clusters can broadly be classified as clusters of *user technology*, *features*, *service infrastructure*, and *social network*. 

### Edges
The graph is directed; an edge ending in an arrow signifies that one variable influences the other, unless otherwise indicated. An edge ending in a diamond square signifies that the variable with the diamond square is a property or subset of the significant other variable. In the single indicated exceptions, variables can also work as moderators, e.g. in the relationship between *mobile phones*, the *power usage*, and the *battery capacity*. The strength of single connections is not indicated within the graph and cannot be analysed.

### Cluster topology
The different clusters feature a different internal topology. Whereas there are no cliques, cluster 1 (*user technology*) is generally speaking of medium low density (25.8% on average, 6% minimum, 56% maximum) and not centralised. Contrary to that, cluster 2 (*features*) is of an extremely centralised nature with a density of 22.5% (minimum 6%, maximum 100 %). This centralization occurs around the variable *feature performance*, highlighting its importance in the analysed phenomenon. In both clusters 3 (*service infrastructure* - average: 18.3%, minimum: 6%, maximum: 31%) and 4 (*social network* - average: 18.6%, minimum: 12%, maximum: 31%) there is little variance regarding the density and no centralization, they rather have the characteristic of a distributed network. 
N.B.: Percentages in this paragraph relate to the centrality of nodes with regards to the number of connected edges in comparison to the globally most connected node in the graph.

Whereas the tools at hand for the analysis of the particular graph did not provide for a calculation of the clustering coefficient directly, from the foregoing figures it can be deduced that this coefficient cannot be large either. Consequently, the network is neither a small-world network, nor is it a regular network. Structural equivalence can neither be observed. When analysing the node-in-betweenness under consideration of the directions of the relationships, the *functionality on different device types* (1.00), *multimedia-preference* (0.97), *mobile phones* (0.78), *microphone quality* and *desktop systems* (0.60) show to be important mediators.

It shall be mentioned that there are also a few outliers; variables, which topically could be expected to be contained within a different cluster but show to be closer related to other clusters. Such are the *user satisfaction* which finds itself in the *service infrastructure* cluster, the *ecosystem integration* variable, which is in the *social network* cluster, the *local infrastructure* in the *features* cluster, and the *economic status* variable in the *user technology* cluster. Analysing the contexts in which the variables were introduced, cluster 4 is the only one in which not variables from all three contexts are present: No technological artefacts independent from the messaging service can be observed within this cluster, although edges from variables within the cluster to such variables in different clusters exist.


### Alternative visualization
A different visualization method for the same graph can reveal further insights on the hierarchy of the variables in the given graph, e.g., the Business Process Model and Notation (see below). It reveals an importance of the *economic status*, the *device type usages*, and *synchronisation capabilities* as highly influential variables, given their placement high in the graph and their high number of edges. Furthermore, it shows how the *multimedia-preference* and the *feature performance* dominate a big part of the variables, visualizing clearer what was previously analysed as high centralization degree and node-in-betweenness-degree. Finally, this notation highlights the importance of broadcasts, groups, the user motivation and the network of contacts on the messenger as ultimate variables of the directed graph.

![Visualization using BPMN](imfactors-visualization-BPMN.png)

## References
Haythornthwaite, C. (1996). Social network analysis: An approach and technique for the study of information exchange. Library and Information Science Research, 18, 323-342