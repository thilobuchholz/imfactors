# Variables
This file contains a list and explanations of all variables used in the graph.

Variables  can be contextualised as either technological artefacts which can be influenced directly by the particular instant messaging service, as technological artefacts independent from the instant messaging service itself, or as variables relating to the user and their attitudes directly.

The variables’ colours in [imfactors.graphml](imfactors.graphml) display their belonging to one of the three established categories (human social properties – yellow, technological artefacts depending on the messaging service – green, technological artefacts independent from the service - orange).

## User-related variables

### User satisfaction
### Motivation to convince others
### Usage on different device types
### Importance of parallel synchronised usage
### Preference for multimedia
### Preference for encryption
### Distrust in companies
### Distrust in government
### Distrust in safe transmission
### Desirability of encryption
### Need for anonymity
### Economic status
-----
## Technical artefacts directly influenced by the respective instant messaging service
### Protocol architecture
### Centralized protocol architecture
### Federated protocol architecture
### Decentralized protocol architecture
### Service uptime
### Address book synchronization
### Notification management
### Power usage
### Functionality on different device types
### Synchronisation capabilities
### Desktop application
### Web application
### Performance of features
### Groups
### Broadcasts
### Forwarding
### Replies
### Pictures
### Files
### Gifs
### Stickers
### Emojis
### Stories
### Voice messages
### Voice calls
### Video calls
### Ephemeral messages
-----
## Independent technical artefacts

### Local infrastructure
### Internet access
### Mobile phones
### Tablets
### Desktop systems
### Operating system
### Operating system type
### Operating system version
### Disk space
### Disk space extensibility
### Memory
### Camera quality
### Microphone quality
### Battery capacity
### Loading time
### Network of contacts
### Ecosystem integration
